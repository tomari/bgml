type tone = A | AX | B | C | CX | D | DX | E | F | FX | G | GX
type note = { tn: tone; length: int }
let playnote n=
  let base_hz=440. in
  let hz=
    match n.tn with
    | A  -> base_hz
    | AX -> base_hz *. (2. ** (1. /. 12.))
    | B  -> base_hz *. (2. ** (2. /. 12.))
    | C  -> base_hz *. (2. ** (3. /. 12.))
    | CX -> base_hz *. (2. ** (4. /. 12. ))
    | D  -> base_hz *. (2. ** (5. /. 12. ))
    | DX -> base_hz *. (2. ** (6. /. 12. ))
    | E  -> base_hz *. (2. ** (7. /. 12. ))
    | F  -> base_hz *. (2. ** (8. /. 12. ))
    | FX -> base_hz *. (2. ** (9. /. 12. ))
    | G  -> base_hz *. (2. ** (10. /. 12. ))
    | GX -> base_hz *. (2. ** (11. /. 12. )) in
  let hz_int=int_of_float hz in
  Graphics.sound hz_int n.length;
  Thread.delay ((float_of_int n.length) /. 1000.)

let rec playsound data=
  match data with
  | [] -> ()
  | hd::tl -> playnote hd; playsound tl

exception Invalid
let playmml str=
  let readmml str=
    let rec ireadmml str pos result=
      if pos<0 then result else
      let note=String.get str pos in
      let ton=
	match note with
	| 'A' -> A
	| 'B' -> B
	| 'C' -> C
	| 'D' -> D
	| 'E' -> E
	| 'F' -> F
	| 'G' -> G
	| _   -> raise Invalid in
      ireadmml str (pos-1) ({ tn=ton; length=333}::result) in
    ireadmml str ((String.length str)-1) [] in
  let trd=Thread.create (fun arg->
    let notes=readmml arg in
    playsound notes) str in
  trd

let bgml_init _=
  Graphics.open_graph ""

