OCAMLC = ocamlc
bgml_test: bgml.cmo bgml_test.ml
	$(OCAMLC) -thread -o bgml_test unix.cma threads.cma graphics.cma $^

bgml.cmo: bgml.ml bgml.cmi
	$(OCAMLC) -thread -c bgml.ml

bgml.cmi: bgml.mli
	$(OCAMLC) -thread $^

clean:
	rm -f *.cm? bgml_test
