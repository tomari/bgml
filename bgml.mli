type tone
type note
val playnote : note -> unit
val playsound : note list -> unit
exception Invalid
val playmml : string -> Thread.t
val bgml_init : 'a -> unit
